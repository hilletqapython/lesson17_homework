import pytest

from lesson17_homework.human import Human


def test_grow(human):
    human.grow()
    assert human.age == 35


@pytest.mark.parametrize("gender, expected_value",
                         [
                            ("female", "female"),
                            ("male", "male"),
                         ]
                         )
def test_change_gender(human, gender, expected_value):
    human.change_gender(gender)
    assert human.gender == expected_value


@pytest.mark.xfail(raises=Exception)
def test_change_gender_exception_xfail(human):
    human.change_gender("male")


def test_change_gender_exception_raises(human):
    with pytest.raises(Exception):
        human.change_gender("male")


def test_gender(human):
    assert human.gender == "male"


def test_age(human):
    assert human.age == 35


def test_human_age(human):
    assert human.age == 35


def test_human_gender(human):
    assert human.gender == "male"


def test_change_gender_exception(human_old):
    with pytest.raises(Exception):
        human_old.change_gender("female")


def test__is_alive_old_human(human_old):
    human_old.grow()
    with pytest.raises(Exception):
        assert Exception == "olga is already dead..."


def test__is_alive_old_human_using_match(human_old):
    with pytest.raises(Exception, match="olga is already dead..."):
        human_old.grow()


def test_gender_old_human(human_old):
    assert human_old.gender == "female"


def test__validate_gender(human):
    with pytest.raises(Exception, match="Not correct name of gender"):
        human.change_gender("attack_helicopter")

