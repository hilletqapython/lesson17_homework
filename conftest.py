import pytest

from lesson17_homework.human import Human


@pytest.fixture(scope="module")
def human() -> Human:
    print()
    print("precondition")

    yield Human(name="Leo", age=34, gender="male")

    print()
    print("teardown")


@pytest.fixture(scope="session")
def human_old() -> Human:
    print()
    print("precondition")

    yield Human(name="olga", age=100, gender="female")

    print()
    print("teardown")

